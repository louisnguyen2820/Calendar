/**
 * @format
 */


import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import DayScreen from './src/screens/DayScreen';
import CalendarsScreen from './src/example/calendars';
import ContainerDayScreen from "./src/screens/ContainerDayScreen";

AppRegistry.registerComponent(appName, () => App);

