/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';


import MainScreen from './screens/MainScreen';
import ZodiacScreen from './screens/ZodiacScreen';
import CustomDrawerContentComponent from './components/CustomDrawerContentComponent';

const Drawer = createDrawerNavigator();


class App extends React.Component{


  render() {
    return (
      <NavigationContainer >
          <Drawer.Navigator initialRouteName="Home" drawerContentOptions={{
            activeTintColor: '#e91e63',
            itemStyle: {marginVertical: 5},
          }}
          drawerContent={(props) => <CustomDrawerContentComponent {...props} />}
          >
            <Drawer.Screen name="MainScreen" component={MainScreen} />
            <Drawer.Screen name="ZodiacScreen" component={ZodiacScreen} />
          </Drawer.Navigator>
        </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'black'
  },
});

export default App;
